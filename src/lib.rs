use anyhow::Result;
use serde::{Deserialize, Serialize};

#[derive(PartialEq, Debug, Serialize, Deserialize)]
pub struct Color {
    pub red: u32,
    pub green: u32,
    pub blue: u32,
}

impl Color {
    pub fn new(red: u32, green: u32, blue: u32) -> Color {
        Color { red, green, blue }
    }

    pub fn red() -> Color {
        Color::new(255, 0, 0)
    }

    pub fn green() -> Color {
        Color::new(0, 255, 0)
    }

    pub fn blue() -> Color {
        Color::new(0, 0, 255)
    }

    pub fn black() -> Color {
        Color::new(0, 0, 0)
    }

    pub fn white() -> Color {
        Color::new(255, 255, 255)
    }

    pub fn gray() -> Color {
        Color::new(128, 128, 128)
    }

    pub fn random() -> Color {
        Color::new(rand::random(), rand::random(), rand::random())
    }

    pub fn to_rgb(&self) -> (u32, u32, u32) {
        (self.red, self.green, self.blue)
    }

    pub fn to_rgba(&self) -> (u32, u32, u32, u32) {
        (self.red, self.green, self.blue, 255)
    }

    pub fn to_hex(&self) -> String {
        format!("#{:02x}{:02x}{:02x}", self.red, self.green, self.blue)
    }

    pub fn to_hex_alpha(&self, alpha: u32) -> String {
        format!(
            "#{:02x}{:02x}{:02x}{:02x}",
            self.red, self.green, self.blue, alpha
        )
    }

    pub fn to_rgba_simplified(&self) -> (f32, f32, f32, f32) {
        (
            self.red as f32 / 255.0,
            self.green as f32 / 255.0,
            self.blue as f32 / 255.0,
            1.0,
        )
    }

    pub fn to_rgba_simplified_alpha(&self, alpha: f32) -> (f32, f32, f32, f32) {
        (
            self.red as f32 / 255.0,
            self.green as f32 / 255.0,
            self.blue as f32 / 255.0,
            alpha,
        )
    }

    pub fn from_hex_str(hex: &str) -> Result<Color> {
        let hex = hex.trim_start_matches('#');
        let r = u32::from_str_radix(&hex[0..2], 16)
            .map_err(|_| anyhow::anyhow!("Invalid hex value"))?;
        let g = u32::from_str_radix(&hex[2..4], 16)
            .map_err(|_| anyhow::anyhow!("Invalid hex value"))?;
        let b = u32::from_str_radix(&hex[4..6], 16)
            .map_err(|_| anyhow::anyhow!("Invalid hex value"))?;
        Ok(Color::new(r, g, b))
    }

    pub fn to_hsl(&self) -> (f32, f32, f32) {
        let r = self.red as f32 / 255.0;
        let g = self.green as f32 / 255.0;
        let b = self.blue as f32 / 255.0;

        let max = f32::max(r, f32::max(g, b));
        let min = f32::min(r, f32::min(g, b));
        let delta = max - min;

        let h = if delta == 0.0 {
            0.0
        } else {
            match max {
                r => (g - b) / delta,
                g => (b - r) / delta + 2.0,
                b => (r - g) / delta + 4.0,
                _ => 0.0,
            }
        };

        let h = h * 60.0;
        let h = if h < 0.0 { h + 360.0 } else { h };

        let l = (max + min) / 2.0;

        let s = if delta == 0.0 {
            0.0
        } else {
            delta / (1.0 - f32::abs(2.0 * l - 1.0))
        };

        (h, s, l)
    }

    pub fn to_hsl_alpha(&self, alpha: f32) -> (f32, f32, f32, f32) {
        let (h, s, l) = self.to_hsl();

        (h, s, l, alpha)
    }

    pub fn from_hsl(h: f32, s: f32, l: f32) -> Color {
        let h = h / 60.0;
        let i = h.floor();
        let f = h - i;
        let p = l * (1.0 - s);
        let q = l * (1.0 - s * f);
        let t = l * (1.0 - s * (1.0 - f));

        let (r, g, b) = match i as u32 {
            0 => (l, t, p),
            1 => (q, l, p),
            2 => (p, l, t),
            3 => (p, q, l),
            4 => (t, p, l),
            5 => (l, p, q),
            _ => (0.0, 0.0, 0.0),
        };

        Color::new((r * 255.0) as u32, (g * 255.0) as u32, (b * 255.0) as u32)
    }

    pub fn similar(&self) -> Color {
        // generate a similar color with RANDOM saturation and lightness closest to the original color
        let (h, _, _) = self.to_hsl();
        let s = rand::random::<f32>() * 0.5 + 0.5;
        let l = rand::random::<f32>() * 0.5 + 0.5;
        Color::from_hsl(h, s, l)
    }

    pub fn is_similar(&self, other: &Color) -> bool {
        let (h, _, _) = self.to_hsl();
        let (oh, _, _) = other.to_hsl();
        (h - oh).abs() < 10.0
    }

    pub fn offset_hue(&self, degrees: f32) -> Color {
        let (h, s, l) = self.to_hsl();
        let h = (h + degrees) % 360.0;

        Color::from_hsl(h, s, l)
    }

    pub fn to_yiq(&self) -> u32 {
        let r = self.red as f32 / 255.0;
        let g = self.green as f32 / 255.0;
        let b = self.blue as f32 / 255.0;

        let yiq = ((r * 299.0) + (g * 587.0) + (b * 114.0)) / 1000.0;
        yiq as u32
    }

    pub fn contrasting(&self, threshold: Option<u32>) -> Color {
        let threshold = threshold.unwrap_or(128);
        let yiq = self.to_yiq();

        if yiq >= threshold {
            Color::black()
        } else {
            Color::white()
        }
    }
}

impl From<(u32, u32, u32)> for Color {
    fn from(color: (u32, u32, u32)) -> Color {
        Color::new(color.0, color.1, color.2)
    }
}

impl From<(u32, u32, u32, u32)> for Color {
    fn from(color: (u32, u32, u32, u32)) -> Color {
        Color::new(color.0, color.1, color.2)
    }
}
