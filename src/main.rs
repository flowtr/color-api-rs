use anyhow::Result;
use color_api::Color;
use warp::Filter;

#[tokio::main]
async fn main() -> Result<()> {
    pretty_env_logger::init();

    // GET /color/similar => 200 OK with body of random color that is similar to the passed in body color in JSON
    let similar_color = warp::path!("color" / "similar")
        .and(warp::post())
        .and(warp::body::json::<Color>())
        .map(|color: Color| {
            let output_color = color.similar();
            warp::reply::json(&output_color)
        });

    let random_color = warp::path!("color" / "random").map(|| {
        let color = Color::random();
        warp::reply::json(&color)
    });

    let contrasting = warp::path!("color" / "complementary")
        .and(warp::post())
        .and(warp::body::json::<Color>())
        .map(|color: Color| {
            let output_color = color.contrasting(None);
            warp::reply::json(&output_color)
        });
    warp::serve(
        similar_color
            .or(random_color)
            .or(contrasting)
            .with(warp::log("color_api")),
    )
    .run(([127, 0, 0, 1], 3030))
    .await;

    Ok(())
}
