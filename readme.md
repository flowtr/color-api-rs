# color-api

This is a color api that is written in Rust. It can also be used as a color utility library.
So far, these are the current routes that are implemented (each route is specified in JSON format with red, green, blue fields):

- `/color/similar` | Generates a color with a similar shade to the one passed into the POST body.
- `/color/random` | Generates a completely random color.

## Prerequisites

- [rustup](https://rustup.rs) with the nightly toolchain installed

## Usage

You can run the api with

```bash
RUST_LOG=debug cargo run
```

after you have cloned this repository.
The api will be available on port 3030.
