FROM docker.io/instrumentisto/rust:nightly-alpine as builder

WORKDIR /build
COPY . .

RUN apk add --no-cache musl-dev pkgconfig openssl-dev && \
    rustup component add rustfmt --toolchain nightly
RUN cargo +nightly build --release

FROM alpine:latest

WORKDIR /app
COPY --from=builder /build/target/release/color-api-server /app/color-api-server

ENV RUST_LOG=debug
CMD /app/color-api-server
