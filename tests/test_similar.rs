#[cfg(test)]
mod test {
    use color_api::Color;

    #[test]
    pub fn test_similar_1() {
        // test the Color:is_similar method with 0-255 values
        let c1 = Color::new(0, 0, 0);
        let c2 = Color::new(25, 25, 25);

        assert!(c1.is_similar(&c2));
    }
}
